(function(){
	'use strict';

	angular
		.module('starter')
		.controller('AchievementController', AchievementController);

	function AchievementController( $http, $ionicPopover, $scope, $stateParams, $state, $ionicLoading, toastr, UtilService )
	{
		// INSTANCIO O CONTROLLER
		var vm = this;
		var apiUrl = 'http://api.steampowered.com/ISteamUserStats/GetSchemaForGame/v2/?key=DA1AC60743CB1C87D710E15CA0F67281&appid=';

		// VARIAVEIS
		vm.title  = '';
		vm.filter = '';
		vm.achievementColor = false;

		// CHAMADA DAS FUNCOES
		vm.openPopover		= openPopover;
		vm.closePopover		= closePopover;
		vm.redirectDetail	= redirectDetail;
		vm.favorite			= favorite;

		$scope.$on( '$ionicView.enter', getGameData );

		// LISTA DE ACHIEVEMENTS, SIMULA O RETORNO DE UMA QUERY
		vm.objList = null;

		$ionicPopover.fromTemplateUrl( 'views/achievementPopover.html', { scope: $scope } ).then( function( popover ){ $scope.popover = popover; } );

		// FUNCOES
		function openPopover ( $event )	{ $scope.popover.show( $event ); }
		function closePopover()			{ $scope.popover.hide();		 }

		function loadingShow(){ $ionicLoading.show( { template: '<ion-spinner icon="dots"></ion-spinner>' } ); };
		function loadingHide(){ $ionicLoading.hide(); };

		function redirectDetail( name )
		{
			$state.go( 'menu.achievementDetail', { achievementName: name } );
		}

		function getGameData()
		{
			var language = JSON.parse( localStorage.getItem( 'config' ) ).language;

			loadingShow();

			vm.achievementColor = JSON.parse( localStorage.getItem( 'config' ) ).achievementColor;

			$http.get( apiUrl + $stateParams.cod + "&l=" + language )
				.success( function( data, status, headers, config ){
					vm.title = !data.game.gameName ? getGameName() : data.game.gameName;
					vm.objList = data.game.availableGameStats.achievements;

					var earned = UtilService.getEarnedAchievements();

					for( var index=0; index<earned.length; index++ )
					{
						angular.forEach( vm.objList, function( list, listIndex )
						{
							if( earned[index].name == list.name )
								list.filter = 'done';						
						});
					}
					loadingHide();
				})
				.error( function( data, status, headers, config ){
					loadingHide();
					toastr.error( 'Erro ao acessar os dados' );
				});

			
		}

		function favorite()
		{ 
			var actualUser = JSON.parse( localStorage.getItem( 'actualUser' ) );

			angular.forEach( actualUser.favorite, function( favorite, index ){
				if( favorite.id == $stateParams.cod )
					actualUser.splice(index, 1);
			});

			toastr.success(vm.title + ' adicionado aos favoritos!'); 
		}
		function getGameName()
		{
			return "Nome Jogo";
		}
	}
})();