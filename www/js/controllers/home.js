(function(){
	'use strict';

	angular
		.module('starter')
		.controller('HomeController', HomeController);

	function HomeController( $http, $scope, $state, $ionicLoading, toastr, UtilService )
	{
		// INSTANCIO O CONTROLLER
		var vm = this;

		// LISTA DE JOGOS DO USUARIO
		vm.objList = [];			

		// CHAMADA DAS FUNCOES
		vm.achievementPage = achievementPage;
		vm.getCompletedAchievement = getCompletedAchievement;
		vm.getTotalAchievement = getTotalAchievement;

		$scope.$on( '$ionicView.enter', getFavoriteCache );

		// FUNCOES
		function achievementPage( list )
		{ 
			UtilService.setEarnedAchievements( list.earned );
			$state.go( 'menu.achievement', { cod: list.id } ); 
		}
		function getFavoriteCache()
		{
			if( localStorage.actualUser )
				vm.objList = JSON.parse( localStorage.getItem( 'actualUser' ) ).favorite;
		}

		function loadingShow(){ $ionicLoading.show( { template: '<ion-spinner icon="dots"></ion-spinner>' } ); };
		function loadingHide(){ $ionicLoading.hide(); };

		function getCompletedAchievement( list )
		{
			var steamApiUrlGame    = 'http://api.steampowered.com/ISteamUserStats/GetUserStatsForGame/v0002/?appid=';
			var steamApiUrlKeyUser = '&key=DA1AC60743CB1C87D710E15CA0F67281&steamid=';
			var userId             = JSON.parse( localStorage.getItem( 'actualUser' ) ).profile;
			var gameId             = list.id;

			loadingShow();
			$http.get( steamApiUrlGame + gameId + steamApiUrlKeyUser + userId )
				.success( function( data, status, headers, config ){
					list.earned = data.playerstats.achievements;
					loadingHide();
				})
				.error( function( data, status, headers, config ){
					loadingHide();
					toastr.error( 'Erro ao acessar os dados' );
				});
		}

		function getTotalAchievement( list )
		{
			var apiUrl = 'http://api.steampowered.com/ISteamUserStats/GetSchemaForGame/v2/?key=DA1AC60743CB1C87D710E15CA0F67281&appid=';
			var gameId = list.id;

			loadingShow();
			$http.get( apiUrl + gameId )
				.success( function( data, status, headers, config ){
					list.total = data.game.availableGameStats.achievements.length;
					loadingHide();
				})
				.error( function( data, status, headers, config ){
					loadingHide();
					toastr.error( 'Erro ao acessar os dados' );
				});
		}
	}
})();