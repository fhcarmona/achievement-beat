(function(){
	'use strict';

	angular
		.module('starter')
		.controller('AchievementDetailController', AchievementDetailController);

	function AchievementDetailController( $http, $scope, $stateParams, $ionicPopup, $timeout, $ionicLoading, toastr )
	{
		// INSTANCIO O CONTROLLER
		var vm = this;
		var achievement

		// VARIAVEIS
		vm.title 		= null;
		vm.description	= null;
		vm.tips			= null;
		vm.steps		= null;
		vm.popup		= null;		

		// CHAMADA DE FUNCOES
		vm.checkAchievement = checkAchievement;
		vm.openPopup		= openPopup;

		$scope.$on( '$ionicView.enter', checkAchievement );

		// FUNCOES
		function checkAchievement()
		{
			var achievementBeatAPI = 'http://localhost:8080/ab-api/tutorialAPI/getTutorial/';

			loadingShow();

			$http.get( achievementBeatAPI + $stateParams.achievementName )
				.success( function( data, status, headers, config ){
					vm.description = data.descricao;
					vm.tips = data.dicas;
					vm.steps = data.passos;

					console.log( data );
					loadingHide();
				})
				.error( function( data, status, headers, config ){
					loadingHide();
					toastr.error( 'Erro ao acessar os dados' );
				});
		}

		function loadingShow(){ $ionicLoading.show( { template: '<ion-spinner icon="dots"></ion-spinner>' } ); };
		function loadingHide(){ $ionicLoading.hide(); };

		function openPopup()
		{
			vm.popup = $ionicPopup.confirm(
				{
					title	: vm.title,
					template: 'Tem certeza que quer baixar este tutorial?'
				});

			vm.popup.then(
				function(retorno)
				{
					if( retorno )
						downloadTutorial();
				}
			);
		}

		function downloadTutorial(){ toastr.success(vm.title + ' baixado!'); }
	}
})();