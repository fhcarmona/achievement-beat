(function(){
	'use strict';

	angular
		.module('starter')
		.controller('MenuController', MenuController);

	function MenuController( $state )
	{
		// INSTANCIO O CONTROLLER
		var vm = this;

		// VARIAVEIS
		vm.redirectFavorite		= redirectFavorite;
		vm.redirectConfig		= redirectConfig;
		vm.redirectDownloads	= redirectDownloads;
		vm.redirectSearch		= redirectSearch;

		// FUNCOES
		function redirectFavorite	(){ $state.go( 'menu.home'				);	}
		function redirectConfig		(){ $state.go( 'menu.config'			);	}
		function redirectDownloads	(){ $state.go( 'menu.downTutorial'		);	}
		function redirectSearch		(){ $state.go( 'menu.searchTutorial'	);	}
	}
	
})();