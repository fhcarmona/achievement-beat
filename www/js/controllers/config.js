(function(){
	'use strict';

	angular
		.module	   ( 'starter' 							  )
		.controller( 'ConfigController', ConfigController );

	function ConfigController( $scope, $http, $ionicPopup, $ionicLoading, toastr )
	{
		// INSTANCIO O CONTROLLER
		var vm					  = this;
		var apiGetPlayerSummaries = 'http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=DA1AC60743CB1C87D710E15CA0F67281&steamids=';

		// VARIAVEIS
		vm.photoDataNetwork = false;
		vm.achievementColor = false;
		vm.photoWifi		= false;
		vm.actualUser;		
		vm.config;
		vm.users;
		vm.language			= 'portuguese';

		// CHAMADA DAS FUNCOES,
		vm.changeProfile = changeProfile;
		vm.deleteProfile = deleteProfile;	
		vm.newProfile	 = newProfile;
		vm.setConfig	 = setConfig;	
		vm.openPopup	 = openPopup;
		vm.searchID		 = '76561198166012118';

		// CARREGA OS DADOS DA PAGINA INICIAL
		$scope.$on( '$ionicView.enter', load );		

		// FUNCOES
		function getUserData( userId )
		{
			loadingShow();
			$http.get( apiGetPlayerSummaries + userId )
				.success( function( data, status, headers, config ){
					// TRATA ERRO DE CODIGO INEXISTENTE
					if( !data.response.players[0] )
					{
						loadingHide();
						toastr.error( 'Código inexistente' );
						return;
					}

					// DADOS DO USUARIO
					var user = { 
						'timeCreated': data.response.players[0].timecreated * 1000,
						   'favorite':  [
											{ id: '489830', name: 'TES: Skyrim',  earned: '75', total: '75', image: 'img/theElderScrollsSkyrim/game.png'	}, 
											{ id: '236430', name: 'Dark Souls 2', earned: '31', total: '38', image: 'img/darkSouls/game.jpg'				},
											{ id: '35720',  name: 'Trine 2', 	  earned: '20', total: '97', image: 'img/trineII/game.png'					}
										],
							'profile':			  data.response.players[0].steamid,
							 'avatar':			   data.response.players[0].avatar,
							   'name':		  data.response.players[0].personaname
					};
				
					user = userStoragePush( user );

					// CONVERTE PARA STRING
					localStorage.setItem( 'users', JSON.stringify( user ) );	

					vm.users = JSON.parse( localStorage.getItem( 'users' ) );

					loadingHide();
				})
				.error( function( data, status, headers, config ){
					loadingHide();
					toastr.error( 'Erro ao acessar os dados' );
				});
		}

		function openPopup()
		{
			vm.popup = $ionicPopup.show({
				template : '<input type="text" ng-model="configCTRL.searchID"/>',
				subTitle : '<h5>Digite o código Steam</h5>',
				scope	 : $scope,
				buttons	 :
				[
					{ text: 'Cancelar' },
					{
						text  : '<b>Pesquisar</b>',
						type  : 'button-positive',
						onTap : function( event )
						{
							if(!vm.searchID)
							{
								toastr.error('Erro. Campo vazio!');
								event.preventDefault();
							}
							else
								newProfile( vm.searchID );
						}
					}
				]
			});
		}

		function loadingShow(){ $ionicLoading.show( { template: '<ion-spinner icon="dots"></ion-spinner>' } ); };
		function loadingHide(){ $ionicLoading.hide(); };

		// CARREGA OS DADOS INICIAIS
		function load()
		{			
			getConfig();	  // CONSULTA O DADOS DE CONFIGURACAO
			getStorageUser(); // CONSULTA OS USER NO LOCALSTORAGE CASO ELE EXISTA
		}

		// CHAMA OS METODOS DE CHECAGEM DO PERFIL E ADICIONAR NOVO PERFIL
		function newProfile( id )
		{
			if( verifySteamId( id ) )
				getUserData( id );
			else
				toastr.error( 'Perfil Existente Na Lista!' );
		}

		// MODIFICA UM PERFIL EXISTENTE
		function changeProfile( user )
		{
			localStorage.setItem( 'actualUser', JSON.stringify( user ) );
			getStorageUser();
			toastr.info( 'Perfil Alterado!' );
		}

		// DELETA UM PERFIL
		function deleteProfile( index )
		{
			var users = JSON.parse( localStorage.getItem( 'users' ) );

			// SE PERFIL ATUAL FOR IGUAL AO PERFIL QUE SERA APAGADO, ENTAO REMOVE O PERFIL 'LOGADO'
			if( localStorage.getItem( 'actualUser' ) )
				if( users[index].profile == JSON.parse( localStorage.getItem( 'actualUser' ) ).profile)
	  				localStorage.removeItem( 'actualUser' );

 			// DELETA O PERFIL
  			users.splice(index, 1);   			

  			// ADICIONA A NOVA LISTA DE PERFIS NO LOCALSTORAGE
  			localStorage.setItem( 'users', JSON.stringify( users ) );
  			
  			// ATUALIZA OS DADOS NECESSARIOS
			getStorageUser();

			// RETORNA MENSAGEM DE EXITO
			toastr.success( 'Perfil Deletado!' );
		}

		// ADICIONA UM NOVO PERFIL A LISTA DE USUARIOS
		function userStoragePush( user )
		{
			// VERIFICA SE EXISTE ALGUM VALOR ANTIGO
			if( localStorage.getItem('users') )
				var newStorage = JSON.parse( localStorage.getItem( 'users' ) );
			else
				var newStorage = [];

			// ADICIONA O NOVO USUARIO A LISTA
			newStorage.push( user );

			// RETORNA A LISTA ATUALIZADA
			return newStorage;
		}

		// VERIFICA SE O ID DO USUARIO EXISTE NA LISTA NO LOCALSTORAGE
		function verifySteamId( id )
		{
			var storage = 0;

			// VERIFICA SE EXISTE LISTA
			if( localStorage.getItem( 'users' ) )
				storage = JSON.parse( localStorage.getItem( 'users' ) );

			// CASO EXISTA LISTA, ENTRA NO LOOP TENTANDO IDENTIFICAR SE EXISTE PERFIL COM O ID DIGITADO
			for( var i=0; i<storage.length; i++)
				if( storage[i].profile == id )
					return false;

			// SE NAO HOUVER PERFIL COM O MESMO ID
			return true; 
		}

		// GET'S
		// ATUALIZA OS DADOS DA CONFIGURACOES
		function getConfig()
		{
			// CASO NAO EXISTA
			if( !localStorage.getItem( 'config' ) )
				setConfig();

			// ATUALIZA OS DADOS DE CONFIGURACAO
			vm.photoDataNetwork = JSON.parse( localStorage.getItem( 'config' ) ).photoDataNetwork;
			vm.photoWifi		= JSON.parse( localStorage.getItem( 'config' ) ).photoWifi;
			vm.language			= JSON.parse( localStorage.getItem( 'config' ) ).language;
			vm.achievementColor	= JSON.parse( localStorage.getItem( 'config' ) ).achievementColor;
		}

		// ATUALIZA OS DADOS DO USUARIO
		function getStorageUser()
		{
			if( localStorage.users )
			{
				vm.users	  = JSON.parse( localStorage.getItem( 'users' 	   ) );
				vm.actualUser = JSON.parse( localStorage.getItem( 'actualUser' ) );
			}
		}

		// SET'S
		// MODIFICA AS CONFIGURACOES NO LOCALSTORAGE
		function setConfig()
		{
			var config = { 'photoDataNetwork': vm.photoDataNetwork, 'photoWifi': vm.photoWifi, 'language': vm.language, 'achievementColor': vm.achievementColor };

			// CONVERTE PARA STRING
			localStorage.setItem( 'config', JSON.stringify( config ) );
		}
	}	
})();