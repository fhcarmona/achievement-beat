(function(){
	'use strict';

	angular
		.module('starter')
		.controller('SearchTutorialController', SearchTutorialController);

	function SearchTutorialController( $state, $http, $ionicPopup, $scope, $ionicLoading, toastr, UtilService )
	{
		// INSTANCIA DO CONTROLLER
		var vm = this;

		// CHAMADA DAS FUNCOES
		vm.openPopup = openPopup;
		vm.search;
		vm.objList = [];

		vm.achievementPage = achievementPage;

		// FUNCOES
		function openPopup()
		{
			vm.popup = $ionicPopup.show({
				template	: '<input type="text" ng-model="searchTutorialCTRL.search"/>',
				subTitle	: 'Defina o jogo a ser encontrado',
				scope		: $scope,
				buttons		:
				[
					{ text: 'Cancelar' },
					{
						text  : '<b>Pesquisar</b>',
						type  : 'button-positive',
						onTap : function( event )
						{
							if(!vm.search)
							{
								toastr.error('Erro. Campo vazio!');
								event.preventDefault();
							}
							else
								getPlayerGames();
						}
					}
				]
			});
		}

		function getPlayerGames()
		{
			var apiUrl = 'http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=DA1AC60743CB1C87D710E15CA0F67281&steamid=';
			var userId = JSON.parse( localStorage.getItem( 'actualUser' ) ).profile;

			if( !userId )
				return;

			loadingShow();
			$http.get( apiUrl + userId + '&format=json' )
				.success( function( data, status, headers, config ){
					angular.forEach( data.response.games, function( row, key ){
						getGameDetail( row.appid );
					});
					
					loadingHide();
				})
				.error( function( data, status, headers, config ){
					loadingHide();
					toastr.error( 'Erro ao acessar os dados' );
				});
		}

		function achievementPage( list )
		{ 
			var steamApiUrlGame    = 'http://api.steampowered.com/ISteamUserStats/GetUserStatsForGame/v0002/?appid=';
			var steamApiUrlKeyUser = '&key=DA1AC60743CB1C87D710E15CA0F67281&steamid=';
			var userId             = JSON.parse( localStorage.getItem( 'actualUser' ) ).profile;
			var gameId             = list.id;

			loadingShow();
			$http.get( steamApiUrlGame + gameId + steamApiUrlKeyUser + userId )
				.success( function( data, status, headers, config ){
					UtilService.setEarnedAchievements( data.playerstats.achievements );
					$state.go( 'menu.achievement', { cod: list.id } ); 
				})
				.error( function( data, status, headers, config ){
					loadingHide();
					toastr.error( 'Erro ao acessar os dados' );
				});
		}

		function getGameDetail( gameId )
		{
			var apiUrl = 'http://store.steampowered.com/api/appdetails?appids=';

			vm.objList = [];

			$http.get( apiUrl + gameId )
				.success( function( data, status, headers, config ){
					if( angular.lowercase( data[ gameId ].data.name ).includes( angular.lowercase( vm.search ) ) )
						vm.objList.push ( {'id': gameId, 'name': data[ gameId ].data.name, 'image': data[ gameId ].data.header_image } );
				})
				.error( function( data, status, headers, config ){
					loadingHide();
					toastr.error( 'Erro ao acessar os dados' );
				});
		}

		function loadingShow(){ $ionicLoading.show( { template: '<ion-spinner icon="dots"></ion-spinner>' } ); };
		function loadingHide(){ $ionicLoading.hide(); };
	}	
})();