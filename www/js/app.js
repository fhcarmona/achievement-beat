(function ()
  {
    'use strict';
    angular
      .module('starter', ['ionic', 'ngAnimate', 'toastr'])
      .run   (runFunction)
      .config(configFunction)

      // FUNCTIONS
      function runFunction($ionicPlatform)
      {
        $ionicPlatform.ready(function()
        {
          if(window.cordova && window.cordova.plugins.Keyboard)
          {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);
          }

          if(window.StatusBar)
            StatusBar.styleDefault();
        });
      }

      function configFunction($urlRouterProvider, $stateProvider)
      {
        $stateProvider
          .state('menu', {
            url         : '',
            abstract    : true,
            templateUrl : 'views/menu.html',
            controller  : 'MenuController',
            controllerAs: 'menuCTRL'
          })
          .state('menu.home', {
            url         : '/principal',
            templateUrl : 'views/home.html',
            controller  : 'HomeController',
            controllerAs: 'homeCTRL'
          })
          .state('menu.achievement', {
            url         : '/achievement/:cod',
            templateUrl : 'views/achievement.html',
            controller  : 'AchievementController',
            controllerAs: 'achievementCTRL'
          })
          .state('menu.achievementDetail', {
            url         : '/achievement/descricao/:achievementName',
            templateUrl : 'views/achievementDetail.html',
            controller  : 'AchievementDetailController',
            controllerAs: 'achievementDetailCTRL'
          })
          .state('menu.config', {
            url         : '/configuracoes',
            templateUrl : 'views/config.html',
            controller  : 'ConfigController',
            controllerAs: 'configCTRL'
          })
          .state('menu.searchTutorial', {
            url         : '/pesquisa',
            templateUrl : 'views/searchTutorial.html',
            controller  : 'SearchTutorialController',
            controllerAs: 'searchTutorialCTRL'
          })
          .state('menu.downTutorial', {
            url         : '/baixados',
            templateUrl : 'views/downTutorial.html',
            controller  : 'DownTutorialController',
            controllerAs: 'downTutorialCTRL'
          })

          $urlRouterProvider.otherwise('/principal');
      }
  }
)();