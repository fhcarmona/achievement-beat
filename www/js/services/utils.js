(function() {
 'use strict';
 
angular
	.module('starter')
	.service('UtilService', UtilService);

	function UtilService()
	{
		var vm = this;
		var earnedAchievements = null;
		
		vm.getEarnedAchievements = getEarnedAchievements;
		vm.setEarnedAchievements = setEarnedAchievements;

		function getEarnedAchievements()
		{
			return earnedAchievements;
		}

		function setEarnedAchievements( value )
		{
			earnedAchievements = value;
		}
	}
})();