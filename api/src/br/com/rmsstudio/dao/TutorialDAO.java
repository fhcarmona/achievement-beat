package br.com.rmsstudio.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.rmsstudio.config.BDConfig;
import br.com.rmsstudio.entidade.TutorialModel;

public class TutorialDAO
{
	public TutorialModel getTutorial( String nomeTutorial ) throws ClassNotFoundException, SQLException
	{
		TutorialModel registroTutorial = null;
		
		Connection conexao = BDConfig.getConnection();
		
		String query = "SELECT * FROM tutorial WHERE tutAchievementName = ?";			
		
		PreparedStatement queryPrepare = conexao.prepareStatement( query );
		
		queryPrepare.setString( 1, nomeTutorial );		
		
		ResultSet resultadoQuery = queryPrepare.executeQuery();		
		
		if( resultadoQuery.next() )
		{
			registroTutorial = new TutorialModel();
			
			registroTutorial.setCodigoTutorial( resultadoQuery.getInt( "tutID" ) );
			registroTutorial.setCodigoGame	  ( resultadoQuery.getInt( "gamID" ) );
			
			registroTutorial.setDescricao( resultadoQuery.getString( "tutDescription" ) );
			registroTutorial.setDicas	 ( resultadoQuery.getString( "tutTips" 		  ) );
			registroTutorial.setPassos	 ( resultadoQuery.getString( "tutSteps" 	  ) );
		}
		
		conexao.close();
		
		return registroTutorial;
	}
}
