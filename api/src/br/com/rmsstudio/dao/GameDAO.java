package br.com.rmsstudio.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.rmsstudio.config.BDConfig;
import br.com.rmsstudio.entidade.GameModel;

public class GameDAO
{
	public GameModel getGame( int gameID ) throws ClassNotFoundException, SQLException
	{
		GameModel registroGame = null;
		
		Connection conexao = BDConfig.getConnection();
		
		String query = "SELECT * FROM game WHERE gamID = ?";			
		
		PreparedStatement queryPrepare = conexao.prepareStatement( query );
		
		queryPrepare.setInt( 1, gameID );		
		
		ResultSet resultadoQuery = queryPrepare.executeQuery();
		
		if( resultadoQuery.next() )
		{
			registroGame = new GameModel();
			
			registroGame.setCodigoGame ( resultadoQuery.getInt( "gamID"      ) );			
			registroGame.setCodigoSteam( resultadoQuery.getInt( "gamSteamID" ) );
			
			registroGame.setNome( resultadoQuery.getString( "gamName" ) );
		}
		
		conexao.close();
		
		return registroGame;
	}
}
