package br.com.rmsstudio.services;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONObject;

import br.com.rmsstudio.dao.GameDAO;
import br.com.rmsstudio.dao.TutorialDAO;
import br.com.rmsstudio.entidade.GameModel;
import br.com.rmsstudio.entidade.TutorialModel;

@Path("/tutorialAPI")
public class achievementBeatService
{
	private TutorialDAO tutorialDAO;
	private GameDAO     gameDAO;
	
	@PostConstruct
	private void init()
	{
		tutorialDAO = new TutorialDAO();
		gameDAO     = new GameDAO();
	}
	
	@GET
	@Path( "/getTutorial/{name}" )
	@Consumes( MediaType.TEXT_PLAIN )
	public Response getTutorial( @PathParam("name") String nomeTutorial )
	{		
		try
		{
			TutorialModel objTutorial = tutorialDAO.getTutorial( nomeTutorial                );
			GameModel     objGame     = gameDAO.getGame        ( objTutorial.getCodigoGame() );

			JSONObject jsonObject = new JSONObject();
			
			jsonObject.put( 	"tutID", String.valueOf( objTutorial.getCodigoTutorial() ) );
			jsonObject.put( "descricao",      objTutorial.getDescricao() );
			jsonObject.put( 	"dicas",          objTutorial.getDicas() );
			jsonObject.put(    "passos",         objTutorial.getPassos() );
			
			jsonObject.put( "steamID", objGame.getCodigoSteam() );
			jsonObject.put(    "nome",        objGame.getNome() );
			
			
			System.out.println( objTutorial );
			System.out.println( jsonObject );
			
			Object teste = new Object[]{
					objTutorial.getCodigoTutorial(), 
					objTutorial.getDescricao(),
					objTutorial.getDicas(),
					objTutorial.getPassos(),
					
					objGame.getCodigoSteam(),
					objGame.getNome()
					};
			
			System.out.println( teste );
			
			return teste;
		}
		catch( Exception error )
		{
			System.out.println( error.getMessage() );
			return null;
		}
	}
}
