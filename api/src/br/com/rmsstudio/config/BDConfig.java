package br.com.rmsstudio.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class BDConfig
{
	public static Connection getConnection() throws ClassNotFoundException, SQLException
	{
		Class.forName( "com.mysql.jdbc.Driver" );
		
		return DriverManager.getConnection( "jdbc:mysql://localhost:3306/achievementbeat?autoReconnect=true&useSSL=false", "root", "123456" );
	}
}
