package br.com.rmsstudio.entidade;

public class GameModel
{
	private int codigoGame;  // CHAVE PRIMARIA DA TABELA GAME
	private int codigoSteam; // CODIGO UNICO DO APLICATIVO NA STEAM
	
	private String nome;     // NOME DO JOGO
	
	// GET'S
	public int getCodigoGame (){ return codigoGame;  }
	public int getCodigoSteam(){ return codigoSteam; }
	
	public String getNome(){ return nome; }	
	
	// SET'S
	public void setCodigoGame ( int codigoGame	){ this.codigoGame  = codigoGame;  }
	public void setNome		  ( String nome		){ this.nome		= nome;		   }
	public void setCodigoSteam( int codigoSteam ){ this.codigoSteam = codigoSteam; }
}
