package br.com.rmsstudio.entidade;

public class TutorialModel
{
	private int codigoTutorial;	// CHAVE PRIMARIA DA TABELA TUTORIAL
	private int codigoGame;		// CHAVE PRIMARIA DA TABELA GAME
	
	private String descricao;	// DESCRICAO DO TUTORIAL
	private String dicas;		// TUTORIAL SEM SPOILER
	private String passos;		// TUTORIAL COM SPOILER
	
	// GET'S
	public int getCodigoTutorial(){ return this.codigoTutorial; }
	public int getCodigoGame    (){ return this.codigoGame;     }
	
	public String getDescricao(){ return this.descricao; }
	public String getDicas    (){ return this.dicas;     }
	public String getPassos   (){ return this.passos;    }
	
	// SET'S
	public void setCodigoTutorial( int valor ){ this.codigoTutorial = valor; }
	public void setCodigoGame    ( int valor ){ this.codigoGame     = valor; }
	
	public void setDescricao( String valor ){ this.descricao = valor; }
	public void setDicas    ( String valor ){ this.dicas     = valor; }
	public void setPassos   ( String valor ){ this.passos    = valor; }
	
}
